<?php

    class Pages extends Controller {
        protected $data;
        protected $products;
        protected $product;

        public function __construct(){
            $this->productModel = $this->model('Product');
        }

        // Data
        public function getData(){
            return $this->data;
        }
        public function setData($data){
            $this->data = $data;
        }

        // Product
        public function getProduct(){
            return $this->product;
        }
        public function setProduct($product){
            $this->product = $product;
        }

        // Products
        public function getProducts(){
            return $this->products;
        }
        public function setProducts($products){
            $this->products = $products;
        }

        // Product List
        public function index(){
            $products = $this->productModel->getProducts();

            $data = [
                'title' => 'Product List',
                'products' => $products
            ];

            if(isset($_POST['deleteBtn'])){
                $recordID = $_POST['recordCheckBox'];

                foreach($recordID as $id){
                    if($_SERVER['REQUEST_METHOD'] == 'POST'){
                        $product = $this->productModel->getProductById($id);
        
                        if($this->productModel->deleteProduct($id)){
                            redirect('pages');
                        } else {
                            die('Something went wrong');
                        }
                    } else {
                        redirect('pages');
                    }
                }

            }

            $this->view('pages/index', $data);
        }

        // Product Add
        public function addproduct(){
            $data = [
                'title' => 'Product Add'
            ];

            if($_SERVER['REQUEST_METHOD'] == 'POST'){
                $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

                $data = [
                    'sku' => trim($_POST['sku']),
                    'name' => trim($_POST['name']),
                    'price' => trim($_POST['price']),
                    'productType' => trim($_POST['productType']),
                    'size' => trim($_POST['size']),
                    'height' => trim($_POST['height']),
                    'width' => trim($_POST['width']),
                    'length' => trim($_POST['length']),
                    'weight' => trim($_POST['weight'])
                ];

                if(!empty($data['sku']) && !empty($data['name']) && !empty($data['price']) && !empty($data['productType'])){
                    if($this->productModel->addProduct($data)){
                        redirect('pages');
                    } else {
                        die('Something went wrong');
                    }
                } else {
                    $this->view('pages/addproduct', $data);
                }
            } else {
                $this->view('pages/addproduct', $data);
            }
        }
    }