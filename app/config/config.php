<?php

    // DB Params
    define('DB_HOST', 'localhost');
    define('DB_USER', 'root');
    define('DB_PASS', 'mysql');
    define('DB_NAME', 'scandiweb');

    // App Root
    define('APPROOT', dirname(dirname(__FILE__)));

    // URL Root
    define('URLROOT', 'http://localhost/test-assignment');

    // Site Name
    define('SITENAME', '');