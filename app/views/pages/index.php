<?php require APPROOT . '/views/inc/header.php'; ?>

    <div class="container">
    <form action="<?php echo URLROOT; ?>/pages/index/<?php echo $data['post']->id; ?>" method='post'>
        <div class="border-bottom border-3 p-2 m-3 d-flex flex-row align-items-center justify-content-between">
            <h1 class='display-6'><?php echo $data['title']; ?></h1>
            <div class="d-flex flex-row flex-wrap justify-content-center align-items-center">
                <a href="<?php echo URLROOT; ?>/pages/addproduct" class='btn btn-outline-success shadow-sm m-1'>ADD</a>    
                <input type='submit' name='deleteBtn' value='MASS DELETE' class='btn btn-outline-danger shadow-sm m-1'>
            </div>
        </div>

        <div class="d-flex flex-row flex-wrap align-items-center justify-content-center">
            <?php foreach($data['products'] as $product) : ?>
                <div class="card shadow-sm border border-1 p-2 m-3 text-center position-relative">
                    <p class='my-1'><?php echo $product->sku; ?></p>
                    <p class='my-1'><?php echo $product->name; ?></p>
                    <p class='my-1'><?php echo $product->price; ?>$</p>
                    <p class='my-1'>
                        <?php
                            if($product->size !== ''){
                                echo 'Size: ' . $product->size . 'MB';
                            } elseif($product->height !== '' && $product->width !== '' && $product->length !== ''){
                                echo 'Dimension: ' . $product->height . 'CM' . 'x' . $product->width . 'CM' . 'x' . $product->length . 'CM';
                            } elseif($product->weight !== ''){
                                echo 'Weight: ' . $product->weight . 'KG';
                            } else {
                                echo 'No measure!!!';
                            }
                        ?>
                    </p>
                    <input type="checkbox" name="recordCheckBox[]" id="recordCheckBox" class='delete-checkbox position-absolute' value='<?php echo $product->id; ?>'>
                </div>
            <?php endforeach; ?>
        </div>
    </form>
    </div>

<?php require APPROOT . '/views/inc/footer.php'; ?>
