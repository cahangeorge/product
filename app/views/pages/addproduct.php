<?php require APPROOT . '/views/inc/header.php'; ?>

    <div class="container">
        <div class="border-bottom border-3 p-2 m-3 d-flex flex-row align-items-center justify-content-between">
            <h1 class='display-6'><?php echo $data['title']; ?></h1>
            <div class="d-flex flex-row align-items-center">
            <form action="<?php echo URLROOT; ?>/pages/addproduct" method='post' id='product_form'>

                <button type='submit' class='btn btn-outline-success shadow-sm m-1'>Save</button>
                <a href="<?php echo URLROOT;?>" class="btn btn-outline-danger shadow-sm m-1">Cancel</a>
            </div>
        </div>

            <div class="form-floating mb-3 shadow-sm mx-auto col-10 col-sm-8 col-md-7 col-lg-6 col-xl-5">
                <input type="text" class="form-control" id="sku" name='sku' required>
                <label for="sku">#sku</label>
            </div>
            <div class="form-floating mb-3 shadow-sm mx-auto col-10 col-sm-8 col-md-7 col-lg-6 col-xl-5">
                <input type="text" class="form-control" id="name" name='name' required>
                <label for="name">#name</label>
            </div>
            <div class="form-floating mb-3 shadow-sm mx-auto col-10 col-sm-8 col-md-7 col-lg-6 col-xl-5">
                <input type="number" class="form-control" id="price" name='price' required>
                <label for="price">#price</label>
            </div>

            <div class="form-floating mx-auto col-10 col-sm-8 col-md-7 col-lg-6 col-xl-5">
                <select class="form-select mb-3 shadow-sm" id="productType" name='productType' required>
                    <option selected disabled></option>
                    <option value="dvd">DVD</option>
                    <option value="furniture">Furniture</option>
                    <option value="book">Book</option>
                </select>
                <label for="productType">Type Switcher</label>
            </div>

            <div class="content mx-auto col-10 col-sm-8 col-md-7 col-lg-6 col-xl-5">
                <div id='dvd' class="data mb-3 mx-auto col-6 col-sm-5 col-md-4 col-lg-3 col-xl-2" style="display: none;">
                    <input id='size' name='size' class="form-control mb-2 shadow-sm" type="number" placeholder="#size">
                </div>

                <div id='furniture' class="data mb-3 mx-auto col-6 col-sm-5 col-md-4 col-lg-3 col-xl-2" style="display: none;">
                    <input id='height' name='height' class="form-control mb-2 shadow-sm" type="number" placeholder="#height">
                    <input id='width' name='width' class="form-control mb-2 shadow-sm" type="number" placeholder="#width">
                    <input id='length' name='length' class="form-control mb-2 shadow-sm" type="number" placeholder="#length">
                </div>

                <div id='book' class="data mb-3 mx-auto col-6 col-sm-5 col-md-4" style="display: none;">
                    <input id='weight' name='weight' class="form-control mb-2 shadow-sm" type="number" placeholder="#weight">
                </div> 
            </div>
        </form>
    </div>

<?php require APPROOT . '/views/inc/footer.php'; ?>