<?php

    abstract class ProductBase {
        protected $db;

        abstract function getProducts();
        abstract function addProduct($data);
        abstract function getProductById($id);
        abstract function deleteProduct($id);
    }

    class Product extends ProductBase {
        protected $data;
        protected $id;

        public function __construct(){
            $this->db = new Database;
        }

        public function getProducts(){
            $this->db->query('SELECT * FROM products');

            $results = $this->db->resultSet();

            return $results;
        }

        public function addProduct($data){
            $this->db->query('INSERT INTO products (sku, name, price, productType, size, height, width, length, weight) VALUES (:sku, :name, :price, :productType, :size, :height, :width, :length, :weight)');

            $this->db->bind(':sku', $data['sku']);
            $this->db->bind(':name', $data['name']);
            $this->db->bind(':price', $data['price']);
            $this->db->bind(':productType', $data['productType']);
            $this->db->bind(':size', $data['size']);
            $this->db->bind(':height', $data['height']);
            $this->db->bind(':width', $data['width']);
            $this->db->bind(':length', $data['length']);
            $this->db->bind(':weight', $data['weight']);

            if($this->db->execute()){
                return true;
            } else {
                return false;
            }
        }

        public function getProductById($id){
            $this->db->query('SELECT * FROM products WHERE id = :id');
            $this->db->bind(':id', $id);

            $row = $this->db->single();

            return $row;
        }

        public function deleteProduct($id){
            $this->db->query('DELETE FROM products WHERE id = :id');
            
            $this->db->bind(':id', $id);

            if($this->db->execute()){
                return true;
            } else {
                return false;
            }
        }
    }